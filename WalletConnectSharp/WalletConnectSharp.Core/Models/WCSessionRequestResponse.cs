namespace EJaw.WalletConnectSharp.Core.Models
{
    public class WCSessionRequestResponse : JsonRpcResponse
    {
        public WCSessionData result;
    }
}