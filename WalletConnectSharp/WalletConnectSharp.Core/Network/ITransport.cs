using EJaw.WalletConnectSharp.Core.Events.Request;
using EJaw.WalletConnectSharp.Core.Events.Response;
using EJaw.WalletConnectSharp.Core.Models;
using System;
using System.Threading.Tasks;

namespace EJaw.WalletConnectSharp.Core.Network
{
    public interface ITransport : IDisposable
    {
        event EventHandler<MessageReceivedEventArgs> MessageReceived;
        
        Task Open(string bridgeURL);

        Task Close();

        Task SendMessage(NetworkMessage message);

        Task Subscribe(string topic);

        Task Subscribe<T>(string topic, EventHandler<JsonRpcResponseEvent<T>> callback) where T : JsonRpcResponse;

        Task Subscribe<T>(string topic, EventHandler<JsonRpcRequestEvent<T>> callback) where T : JsonRpcRequest;
    }
}