using EJaw.WalletConnectSharp.Core.Models;

namespace EJaw.WalletConnectSharp.Core.Events.Response
{
    public class JsonRpcRequestEvent<T> : GenericEvent<T> where T : JsonRpcRequest
    {
    }
}