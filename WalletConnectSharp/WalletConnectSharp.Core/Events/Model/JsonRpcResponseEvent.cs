using EJaw.WalletConnectSharp.Core.Models;

namespace EJaw.WalletConnectSharp.Core.Events.Request
{
    public class JsonRpcResponseEvent<T> : GenericEvent<T> where T : JsonRpcResponse
    {
    }
}