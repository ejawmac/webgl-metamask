using EJaw.Networking.CryptoWallet.ServerSide;
using EJaw.Networking.Websocket;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using UnityEngine;
using UniRx;

namespace EJaw.WalletConnectSharp.Unity
{
    public class WalletConnector
    {
        private const int _Port = 8084;

        private Client _client;
        private HttpServer _webGLServer;
        private Server _server;
        private Process _process;
        private Action<string> _onMessageGot;
        private IDisposable _updater;

        public WalletConnector()
        {
            _server = new Server();
            _server.Start();

            _client = new Client();
            _client.Start();

            string startingPath = Path.Combine(Application.streamingAssetsPath, "WebGL");
            _webGLServer = new HttpServer(startingPath, _Port);
            _webGLServer.Start();

            Update();
        }

        public void Connect(Action<string> onMessageGot)
        {
            _onMessageGot = onMessageGot;

            _process = new Process();
            _process.StartInfo.FileName = @"chrome";
            _process.StartInfo.Arguments = $"http://localhost:8004/ --new-window";
            _process.Start();
        }

        public void Stop()
        {
            if (_server != null)
            {
                _server.Dispose();
            }

            if (_webGLServer != null)
            {
                _webGLServer.Stop();
            }

            if (_client != null)
            {
                _client.Dispose();
            }

            if (_process != null)
            {
                _process.Dispose();
            }

            if (_updater != null)
            {
                _updater.Dispose();
            }
        }

        private void Update()
        {
            _updater = Observable.EveryUpdate()
                .Subscribe(_ =>
                {
                    DoUpdate();
                });
        }

        private void DoUpdate()
        {
            lock (_client.Queue)
            {
                if (_client != null && _client.Queue.Any())
                {
                    var message = _client.Queue.Dequeue();
                    _onMessageGot(message);
                }
            }
        }
    }
}


