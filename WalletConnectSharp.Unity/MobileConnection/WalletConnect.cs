using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Networking;
using EJaw.WalletConnectSharp.Unity.Models;
using EJaw.WalletConnectSharp.Unity.Network;
using EJaw.WalletConnectSharp.Unity.Utils;
using EJaw.Extension;
using EJaw.WalletConnectSharp.Core.Models;
using EJaw.WalletConnectSharp.Core;

namespace EJaw.WalletConnectSharp.Unity
{
    [RequireComponent(typeof(NativeWebSocketTransport))]
    public class WalletConnect : BindableMonoBehavior
    {
        [Serializable] public class ConnectedEventNoSession : UnityEvent { }
        [Serializable] public class ConnectedEventWithSession : UnityEvent<WCSessionData> { }

        public Wallets DefaultWallet;
        public bool persistThroughScenes = true;
        public bool waitForWalletOnStart = true;
        public string customBridgeUrl;
        public int chainId = 1;
        public ConnectedEventNoSession ConnectedEvent;
        public ConnectedEventWithSession ConnectedEventSession;

        [SerializeField] private Button LoginButton;
        [SerializeField] private ClientMeta AppData;

        [BindComponent] private NativeWebSocketTransport _transport;

        public WalletConnectUnitySession ActiveSession => Session;
        public string ConnectURL => Protocol.URI;
        public WalletConnectUnitySession Session { get; private set; }
        public WalletConnectUnitySession Protocol { get; private set; }
        public bool Connected => Session.Connected;
        public Dictionary<string, AppEntry> SupportedWallets { get; private set; }
        public AppEntry SelectedWallet { get; set; }

        protected override void Awake()
        {
            base.Awake();

            if (string.IsNullOrWhiteSpace(customBridgeUrl))
            {
                customBridgeUrl = null;
            }

            Session = new WalletConnectUnitySession(AppData, this, customBridgeUrl, _transport, null, chainId);

            if (DefaultWallet != Wallets.None)
            {
                StartCoroutine(SetupDefaultWallet());
            }

#if UNITY_ANDROID || UNITY_IOS
            //Whenever we send a request to the Wallet, we want to open the Wallet app
            Session.OnSend += (sender, session) => OpenMobileWallet();
#endif

            if (waitForWalletOnStart)
            {
                StartConnect();
                OnSocketConnected();
            }
        }

        public SavedSession SaveSession()
        {
            return Session.SaveSession();
        }

        public void SaveSession(Stream stream, bool leaveStreamOpen = true)
        {
            Session.SaveSession(stream, leaveStreamOpen);
        }

        public void ResumeSession(SavedSession data)
        {
            Session = new WalletConnectUnitySession(data, this, _transport);
            chainId = Session.ChainId;

#if UNITY_ANDROID || UNITY_IOS
            //Whenever we send a request to the Wallet, we want to open the Wallet app
            Session.OnSend += (sender, session) => OpenMobileWallet();
#endif

            if (waitForWalletOnStart)
            {
                StartConnect();
            }
        }

        public void ResumeSession(Stream stream, bool leaveStreamOpen = true)
        {
            ResumeSession(WalletConnectSession.ReadSession(stream, leaveStreamOpen));
        }

        public void StartConnect()
        {
            ConnectedEventWithSession allEvents = new ConnectedEventWithSession();

            allEvents.AddListener(delegate (WCSessionData arg0)
            {
                ConnectedEvent.Invoke();
                ConnectedEventSession.Invoke(arg0);
            });

            WaitForWalletConnection(allEvents);
        }

        public void WaitForWalletConnection(UnityEvent<WCSessionData> onConnected)
        {
            StartCoroutine(ConnectAsync(onConnected));
        }

        public IEnumerator FetchWalletList(bool downloadImages = true)
        {
            using (UnityWebRequest webRequest = UnityWebRequest.Get("https://registry.walletconnect.org/data/wallets.json"))
            {
                // Request and wait for the desired page.
                yield return webRequest.SendWebRequest();

                if (webRequest.result == UnityWebRequest.Result.ConnectionError)
                {
                    Debug.Log("Error Getting Wallet Info: " + webRequest.error);
                }
                else
                {
                    var json = webRequest.downloadHandler.text;

                    SupportedWallets = JsonConvert.DeserializeObject<Dictionary<string, AppEntry>>(json);

                    if (downloadImages)
                    {
                        foreach (var id in SupportedWallets.Keys)
                        {
                            yield return DownloadImagesFor(id);
                        }
                    }
                }
            }
        }

        public async void Disconnect()
        {
            if (!Session.Connected)
                return;

            await Session.Disconnect();
        }

        public void OpenMobileWallet(AppEntry selectedWallet)
        {
            SelectedWallet = selectedWallet;
            OpenMobileWallet();
        }

        public void OpenDeepLink(AppEntry selectedWallet)
        {
            SelectedWallet = selectedWallet;
            OpenDeepLink();
        }

        public void OpenMobileWallet()
        {
#if UNITY_ANDROID
            var signingURL = ConnectURL.Split('@')[0];
            Application.OpenURL(signingURL);

#elif UNITY_IOS
            if (SelectedWallet == null)
            {
                throw new NotImplementedException(
                    "You must use OpenMobileWallet(AppEntry) or set SelectedWallet on iOS!");
            }
            else
            {
                string url;
                string encodedConnect = WebUtility.UrlEncode(ConnectURL);
                if (!string.IsNullOrWhiteSpace(SelectedWallet.mobile.universal))
                {
                    url = SelectedWallet.mobile.universal + "/wc?uri=" + encodedConnect;
                }
                else
                {
                    url = SelectedWallet.mobile.native + (SelectedWallet.mobile.native.EndsWith(":") ? "//" : "/") +
                          "wc?uri=" + encodedConnect;
                }

                var signingUrl = url.Split('?')[0];
                
                Debug.Log("Opening: " + signingUrl);
                Application.OpenURL(signingUrl);
            }
#else
            Debug.Log("Platform does not support deep linking");
            return;
#endif
        }

        public void OpenDeepLink()
        {
#if UNITY_ANDROID
            Application.OpenURL(ConnectURL);

#elif UNITY_IOS
            if (SelectedWallet == null)
            {
                throw new NotImplementedException(
                    "You must use OpenDeepLink(AppEntry) or set SelectedWallet on iOS!");
            }
            else
            {
                string url;
                string encodedConnect = WebUtility.UrlEncode(ConnectURL);
                if (!string.IsNullOrWhiteSpace(SelectedWallet.mobile.universal))
                {
                    url = SelectedWallet.mobile.universal + "/wc?uri=" + encodedConnect;
                }
                else
                {
                    url = SelectedWallet.mobile.native + (SelectedWallet.mobile.native.EndsWith(":") ? "//" : "/") +
                          "wc?uri=" + encodedConnect;
                }
                
                Debug.Log("Opening: " + url);
                Application.OpenURL(url);
            }
#else
            Debug.Log("Platform does not support deep linking");
            return;
#endif
        }

        private IEnumerator ConnectAsync(UnityEvent<WCSessionData> onConnected)
        {
            Debug.Log("Waiting for Wallet connection");

            var connectTask = Task.Run(() => Session.SourceConnectSession());

            var coroutineInstruction = new WaitForTaskResult<WCSessionData>(connectTask);
            yield return coroutineInstruction;

            var task = coroutineInstruction.Source;

            if (task.Exception != null)
            {
                throw task.Exception;
            }

            onConnected.Invoke(task.Result);

            SaveAccount();
        }

        private IEnumerator DownloadImagesFor(string id, string[] sizes = null)
        {
            if (sizes == null)
            {
                sizes = new string[] { "sm", "md", "lg" };
            }

            var data = SupportedWallets[id];

            foreach (var size in sizes)
            {
                var url = "https://registry.walletconnect.org/logo/" + size + "/" + id + ".jpeg";

                using (UnityWebRequest imageRequest = UnityWebRequestTexture.GetTexture(url))
                {
                    yield return imageRequest.SendWebRequest();

                    if (imageRequest.result == UnityWebRequest.Result.ConnectionError)
                    {
                        Debug.Log("Error Getting Wallet Icon: " + imageRequest.error);
                    }
                    else
                    {
                        var texture = ((DownloadHandlerTexture)imageRequest.downloadHandler).texture;
                        var sprite = Sprite.Create(texture,
                            new Rect(0.0f, 0.0f, texture.width, texture.height),
                            new Vector2(0.5f, 0.5f), 100.0f);

                        if (size == "sm")
                        {
                            data.smallIcon = sprite;
                        }
                        else if (size == "md")
                        {
                            data.medimumIcon = sprite;
                        }
                        else if (size == "lg")
                        {
                            data.largeIcon = sprite;
                        }
                    }
                }
            }
        }

        private void SaveAccount()
        {
            string Account = ActiveSession.Accounts[0];
            PlayerPrefs.SetString("Account", Account);
        }

        private async void OnSocketConnected()
        {
            bool connected = _transport.Opened;
            while (!connected)
            {
                await Task.Delay(500);
                connected = _transport.Opened;
            }

            LoginButton.interactable = true;
        }

        private IEnumerator SetupDefaultWallet()
        {
            yield return FetchWalletList(false);

            var wallet = SupportedWallets.Values.FirstOrDefault(a => a.name.ToLower() == DefaultWallet.ToString().ToLower());

            if (wallet != null)
            {
                yield return DownloadImagesFor(wallet.id);
                SelectedWallet = wallet;
                Debug.Log("Setup default wallet " + wallet.name);
            }
        }

        private async void OnDestroy()
        {
            if (!Session.Connected)
                return;

            await Session.Disconnect();
        }

        private async void OnApplicationQuit()
        {
            if (ActiveSession != null)
            {
                await ActiveSession.Disconnect();
                Debug.Log("Mobile Wallet Disonnected");
            }
        }
    }
}