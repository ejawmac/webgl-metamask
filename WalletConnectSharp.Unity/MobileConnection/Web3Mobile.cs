﻿using System.Threading.Tasks;
using System.Numerics;
using EJaw.WalletConnectSharp.Unity;
using EJaw.WalletConnectSharp.Core.Models.Ethereum;

namespace EJaw.Web3mobile
{
    public class Web3Mobile
    {
        public async static Task<string> SendTransaction(WalletConnect walletConnect, string _to, string _value, string _gas, string _data)
        {
            string address = walletConnect.ActiveSession.Accounts[0];

            BigInteger bnValue = BigInteger.Parse(_value);
            string hexVal = "0x" + bnValue.ToString("X");

            BigInteger bnGas = BigInteger.Parse(_gas);
            string hexGas = "0x" + bnGas.ToString("X");

            var transaction = new TransactionData()
            {
                from = address,
                to = _to,
                value = hexVal,
                gas = hexGas,
                data = _data,
            };

            string receipt = await walletConnect.ActiveSession.EthSendTransaction(transaction);

            return receipt;
        }
    }
}
